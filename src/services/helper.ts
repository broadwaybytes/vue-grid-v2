import { ColumnModel } from "../models/TableModels";

export function formatMoney(num: Number | null){
    if (num === null){
        return null;
    }

    return num.toLocaleString('en-US', {
    style: 'currency',
    currency: 'USD',
    minimumFractionDigits: 2, // Specify the number of decimal places (yy)
    maximumFractionDigits: 2, // Specify the number of decimal places (yy)
  });
}

export function getColumns() : ColumnModel[]{
    var arr: ColumnModel[] =  [{ key: 'id', type: 'int', label: 'ID', sortOrder: null},
    { key: 'issuer_name', type: 'string', label: 'Issuer', sortOrder: null },
    { key: 'deal_name', type: 'string', label: 'Deal', sortOrder: null },
    { key: 'bloomberg_id', type: 'string', label: 'Bloomberg ID',sortOrder: null },
    { key: 'total', type: 'money', label: 'Total',sortOrder: null },
    { key: 'industry', type: 'string', label: 'Industry',sortOrder: null },
    { key: 'status', type: 'string', label: 'Status',sortOrder: null },
    { key: 'analysts', type: 'string[]', label: 'Analysts',sortOrder: null },
    { key: 'doc_count', type: 'int', label: 'Docs',sortOrder: null },
    { key: 'custom_deal_identifiers', type: 'string[]', label: 'Identifiers',sortOrder: null }];

    return arr;
}

export function getData(count: number) : []{
    var arr = [];
    
    for(var i=0; i< count; i++){
        arr.push({
            id: {value: i, display: i}, 
            issuer_name: {value: `issuer ${i}`, display:`issuer ${i}`}, 
            deal_name: {value: `deal ${i}`, display: `deal ${i}`}, 
            bloomberg_id: {value: `bloomberg ${i}`, display: `bloomberg ${i}`}, 
            total: {value: 1000.986 + i, display: 1000.986 + i},
            industry: {value: `industry ${i}`, display: `industry ${i}`},
            status: {value: `status ${i}`, display: `status ${i}`},            
            analysts: {value: [`a ${i} one`, `a ${i} two`], display: ''},
            doc_count: {value: i * 2, display: i * 2},
            custom_deal_identifiers: { value: [`c ${i} one`, `c ${i} two`], display: ''}
        });
    }
    
    if (count > 5){
        arr[0].total.value = null!; arr[0].total.display = null!;
        arr[1].status.value = null!; arr[1].status.display = null!;
        arr[3].analysts.value = [`a3 one`, `a3 two`,`a3 three`, `a3 five`, `a3 six`, `a3 seven`,`a3 eight`, `a3 nine`, `a3 ten`, `a3 eleven`];
    }

    return arr as [];
}

export function sortByDataType(val_a:any, val_b:any, direction:string|null, dataType:string) {
    var dir = 0;
    if (direction === 'asc'){
        dir = 1;
    }else if (direction === 'dsc'){
        dir = -1;
    }
    
    if (val_a === null && val_b !== null) {
        return -1 * dir;
    }else if (val_a !== null && val_b === null){
        return 1 * dir;
    }else if (val_a === null && val_b === null){
        return 0;
    }
  

    if (dataType === 'string' || dataType === 'string[]') {
        if (val_a < val_b)
            return -1 * dir;
        if (val_a > val_b)
            return 1 * dir;
        return 0;
    }

    if (dataType === 'date') {
        val_a = Date.parse(val_a);
        val_b = Date.parse(val_b);
    }else if (dataType === 'int') {
        val_a = parseInt(val_a);
        val_b = parseInt(val_b);
    }else if (dataType === 'float' || dataType === 'decimal') {
        val_a = parseFloat(val_a);
        val_b = parseFloat(val_b);
    }else if (dataType === 'bool'){  
        val_a = val_a == true?0:1
        val_b = val_b == true?0:1
    }

    return (val_a - val_b) * dir ;
}

