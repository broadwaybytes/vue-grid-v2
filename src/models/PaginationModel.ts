export class PaginationModel {
    countPerPage: number;
    pageNumber: number;

    constructor(){
        this.countPerPage = 10;
        this.pageNumber = 1;    
    }
}
