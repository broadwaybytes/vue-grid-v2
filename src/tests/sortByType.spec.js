import { sortByDataType } from '../services/helper.ts'
import { describe, it, expect } from 'vitest';

describe('sortByDataType function', () => {
  it('1 Sort strings', () => {
    expect(sortByDataType("apple", "banana", "asc", "string") < 0).toBe(true);
    expect(sortByDataType("apple", "banana", "dsc", "string") > 0).toBe(true);
  });
  
  it('2 Sort dates', () => {
    expect(sortByDataType("2022-01-15", "2023-03-10", "asc", "date") < 0).toBe(true);
    expect(sortByDataType("2022-01-15", "2023-03-10", "dsc", "date") > 0).toBe(true);
  });

  it('3 Sort integers', () => {
    expect(sortByDataType("42", "24", "asc", "int") > 0).toBe(true);
    expect(sortByDataType("42", "24", "dsc", "int") < 0).toBe(true);
  });

  it('4 Sort floats', () => {
    expect(sortByDataType("3.14", "2.718", "asc", "float") > 0).toBe(true);
    expect(sortByDataType("3.14", "2.718", "dsc", "float") < 0).toBe(true);
  });

  it('5 Sort booleans', () => {
    expect(sortByDataType(true, false, "asc", "bool") < 0 ).toBe(true);
    expect(sortByDataType(true, false, "dsc", "bool") > 0).toBe(true);
  });

  it('6 Sort null values', () => {
    expect(sortByDataType(null, "value", "asc", "string") < 0).toBe(true);
    expect(sortByDataType(null, "value", "dsc", "string") > 0).toBe(true);
    expect(sortByDataType(null, null, "asc", "string") === 0).toBe(true);
  });

  it('7 Sorting same string Values', () => {
    expect(sortByDataType("string", "string", "asc", "string") === 0).toBe(true);
  });
});