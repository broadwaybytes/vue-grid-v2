import { mount } from '@vue/test-utils';
import Table from '../components/Table.vue'; 
import { describe, it, expect, beforeEach, afterEach } from 'vitest';

describe('Table.vue', () => {
  let wrapper;
  let columns;
  let items;

  beforeEach(() => {
    columns = [
      { key: 'id', type: 'int', label: 'ID', sortOrder: null},
      { key: 'deal_name', type: 'string', label: 'Deal', sortOrder: null }        
    ];

    items = [
      { id: {value:1, display: 1}, deal_name: {value: 'd 1', key: 'd 1'}},
      { id: {value:2, display: 2}, deal_name: {value: 'd 2', key: 'd 2'}},
      { id: {value:3, display: 3}, deal_name: {value: 'd 3', key: 'd 3'}},
      { id: {value:4, display: 4}, deal_name: {value: 'd 4', key: 'd 4'}},
    ];

    wrapper = mount(Table, {
      props: {
        columns_: columns,
        items_: items,
      },
    });
  });

  afterEach(() => {
    wrapper.unmount();
  });

  it('renders a table with the correct number of rows and columns', () => {  
    expect(wrapper.findAll('tr').length).toBe(5); 
    expect(wrapper.findAll('th').length).toBe(3); 
  });

  it('filters items correctly based on input', async () => {
    await wrapper.setData({ filterText: 'd 4' });

    expect(wrapper.vm.filteredItems.length).toBe(1); 
  });

  
  it('sorts items correctly when a column is clicked', async () => {
    await wrapper.find('.table-header-cell').trigger('click');

    expect(wrapper.vm.items[0].id.value === 1).toBe(true);
    expect(wrapper.vm.items[1].id.value === 2).toBe(true);
  });
});