# Vue Grid with Details Pane 

[live demo](https://broadwaybytes.gitlab.io/vue-grid-v2)

<img src="images/vue-grid-01.png"></image>


## Build Instructions
   1. install: npm install
   2. run dev env: npm dev 
   3. build production: npm build
   4. run unit test: npm run test:unit

## input parameters
   1. columns; array of column object with key, label, and type.
   2. items; array of data with value and display.
   3. width; number of pixels. 
   4. height; number of pixels.

## Features
   1. sorting
   2. filtering
   3. details pane visible after clicking checkbox
   4. pagination
   
## Work Left to be Done. 
   1. styling on the details pane to display data better, especially when the array list gets long
   2. hover-over grid cells to show actual value 

